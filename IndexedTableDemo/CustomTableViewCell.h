//
//  CustomTableViewCell.h
//  IndexedTableDemo
//
//  Created by Rifat Firdaus on 6/30/15.
//  Copyright (c) 2015 Appcoda. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CustomTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *imgAnimal;
@property (weak, nonatomic) IBOutlet UILabel *lblAnimal;

@end
