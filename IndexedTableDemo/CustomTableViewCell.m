//
//  CustomTableViewCell.m
//  IndexedTableDemo
//
//  Created by Rifat Firdaus on 6/30/15.
//  Copyright (c) 2015 Appcoda. All rights reserved.
//

#import "CustomTableViewCell.h"

@implementation CustomTableViewCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
